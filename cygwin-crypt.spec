%{?cygwin_package_header}

Name:           cygwin-crypt
Version:        2.1
Release:        1%{?dist}
Summary:        Cygwin crypt library

License:        GPLv2+
Group:          Development/Libraries
URL:            http://cygwin.com/
BuildArch:      noarch

# git archive of git://sourceware.org/git/cygwin-apps/crypt.git
Source0:        crypt-%{version}.tar.xz

BuildRequires:  cygwin32-filesystem
BuildRequires:  cygwin32-gcc
BuildRequires:  cygwin32-binutils

BuildRequires:  cygwin64-filesystem
BuildRequires:  cygwin64-gcc
BuildRequires:  cygwin64-binutils


%description
crypt(3) library for Cygwin toolchains.

%package -n cygwin32-crypt
Summary:        Cygwin32 crypt library.
Group:          Development/Libraries

%description -n cygwin32-crypt
crypt(3) library for Cygwin i686 cross-compiler toolchain.

%package -n cygwin32-crypt-static
Summary:        Static version of the Cygwin32 crypt library.
Group:          Development/Libraries
Requires:       cygwin32-crypt = %{version}-%{release}

%description -n cygwin32-crypt-static
Static version of the Cygwin i686 crypt(3) library.

%package -n cygwin64-crypt
Summary:        Cygwin64 crypt library.
Group:          Development/Libraries

%description -n cygwin64-crypt
crypt(3) library for Cygwin x86_64 cross-compiler toolchain.

%package -n cygwin64-crypt-static
Summary:        Static version of the Cygwin64 crypt library.
Group:          Development/Libraries
Requires:       cygwin64-crypt = %{version}-%{release}

%description -n cygwin64-crypt-static
Static version of the Cygwin x86_64 crypt(3) library.

%{?cygwin_debug_package}


%prep
%setup -q -n crypt-%{version}


%build
mkdir -p build_32bit
pushd build_32bit
ln -s ../*.* ../Makefile .
make %{?_smp_flags} \
  CC=%{cygwin32_cc} \
  AR=%{cygwin32_ar} \
  CFLAGS="%{cygwin32_cflags}"
popd

mkdir -p build_64bit
pushd build_64bit
ln -s ../*.* ../Makefile .
make %{?_smp_flags} \
  CC=%{cygwin64_cc} \
  AR=%{cygwin64_ar} \
  CFLAGS="%{cygwin64_cflags}"
popd


%install
pushd build_32bit
  make install DESTDIR=$RPM_BUILD_ROOT prefix=%{cygwin32_prefix}
popd
pushd build_64bit
  make install DESTDIR=$RPM_BUILD_ROOT prefix=%{cygwin64_prefix}
popd

# docs will be installed later with %%doc
rm -fr $RPM_BUILD_ROOT%{cygwin32_docdir}
rm -fr $RPM_BUILD_ROOT%{cygwin64_docdir}

# Remove Cygwin executables
rm -f $RPM_BUILD_ROOT%{cygwin32_bindir}/*.exe
rm -f $RPM_BUILD_ROOT%{cygwin64_bindir}/*.exe


%files -n cygwin32-crypt
%doc crypt.README
%{cygwin32_bindir}/cygcrypt-0.dll
%{cygwin32_includedir}/crypt.h
%{cygwin32_libdir}/libcrypt.dll.a

%files -n cygwin32-crypt-static
%{cygwin32_libdir}/libcrypt.a

%files -n cygwin64-crypt
%doc crypt.README
%{cygwin64_bindir}/cygcrypt-0.dll
%{cygwin64_includedir}/crypt.h
%{cygwin64_libdir}/libcrypt.dll.a

%files -n cygwin64-crypt-static
%{cygwin64_libdir}/libcrypt.a


%changelog
* Tue Dec 05 2017 Yaakov Selkowitz <yselkowi@redhat.com> - 2.1-1
- new version

* Mon Sep 12 2016 Yaakov Selkowitz <yselkowi@redhat.com> - 1.4-1
- new version

* Wed Mar 30 2016 Yaakov Selkowitz <yselkowi@redhat.com> - 1.3-1
- new version

* Mon Jul 01 2013 Yaakov Selkowitz <cygwin-ports-general@lists.sourceforge.net> - 1.1-2
- Rebuild for new Cygwin packaging scheme.
- Add cygwin64 package.

* Sun Mar 13 2011 Yaakov Selkowitz <cygwin-ports-general@lists.sourceforge.net> - 1.1-1
- Initial RPM release.
