set -e
set -x

n=crypt
eval v=$(awk '/^Version/{print $2}' cygwin-crypt.spec)

git clone -b crypt-${v/./_}-release git://sourceware.org/git/cygwin-apps/crypt.git

mv $n $n-$v
tar --exclude-vcs -Jcf $n-$v.tar.xz $n-$v/
rm -fr $n-$v/
